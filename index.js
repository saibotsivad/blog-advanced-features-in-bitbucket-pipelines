/*
This is a fabricated demo to show how environment
variables are handled in Bitbucket Pipelines.

In practice you might reference environment variables
as part of your deployment process, for example your
AWS or Azure keys might be set as environment variables
and used in code as part of the deployment.
*/

console.log('Set in the YAML file, not secured:', process.env.FROM_YAML_NOT_SECURE)
console.log('Set in Bitbucket configuration, not secured:', process.env.FROM_BITBUCKET_NOT_SECURE)
console.log('Set in Bitbucket configuration, secured:', process.env.FROM_BITBUCKET_SECURE)

if (process.env.FROM_BITBUCKET_SECURE === 'my-secure-string') {
	console.log('The environment variable is set.')
}
console.log('When it is used as a string: my-secure-string')

const lower = require('private-blog-demo')
console.log(lower('THIS TEXT WILL BE LOWER CASE'))
